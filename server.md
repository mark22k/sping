# Debian

Install ruby and msgpack, which is needed for message encoding.
```sh
$ sudo apt-get -y install ruby ruby-msgpack
```

Download and install sping:
```sh
$ wget https://codeberg.org/mark22k/sping/archive/main.tar.gz -O sping.tar.gz
$ tar xf sping.tar.gz
$ sudo mv sping /var/lib/sping/
$ rm sping.tar.gz
```

If you have previously installed sping, you may have to delete sping before copying:
```sh
$ sudo rm -r /var/lib/sping/
```

Install sping as a systemd service:
```sh
$ sudo adduser --home /var/lib/sping/ --disabled-password --disabled-login --comment "" sping
$ sudo cp /var/lib/sping/contrib/sping.service /etc/systemd/system/
$ sudo systemctl daemon-reload
```
The systemd service was created as securely as possible and uses various security mechanisms (such as `NoNewPrivileges` or `PrivateDevices`). It achieves a score of 1.2 at `systemd-analyze security sping`.

Allow the following ports in your firewall:
- 6924/tcp
- 6924/udp

With ufw you can do this with the following command:
```sh
ufw allow 6924/tcp
ufw allow 6924/udp
```

With nftables you can allow the two ports with the following statments:
```nft
tcp dport 6924 accept;
udp dport 6924 accept;
```

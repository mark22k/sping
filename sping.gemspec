# frozen_string_literal: true
# sharable_constant_value: literal

Gem::Specification.new do |spec|
  spec.name = 'sping'
  spec.version = '1.1.4'
  spec.summary = 'Program for measuring asymmetric latencies and asymmetric packet loss.'
  spec.description = 'This is a reimplementation in Ruby of the reference implementation of the sping protocol in Go. The program provides both the client and server part to measure asymmetric latencies and asymmetric packet loss between two peers.'
  spec.authors = ['Marek Küthe']
  spec.email = 'm.k@mk16.de'

  spec.files = %w[lib/session.rb lib/session_manager.rb lib/last_acks.rb lib/errors.rb]
  spec.executables = %w[sping]
  spec.extra_rdoc_files = %w[LICENSE README.md]

  spec.homepage = 'https://codeberg.org/mark22k/sping'
  spec.license = 'GPL-3.0-or-later'

  spec.metadata = { 'source_code_uri' => 'https://codeberg.org/mark22k/sping',
                    'bug_tracker_uri' => 'https://codeberg.org/mark22k/sping/issues',
                    'rubygems_mfa_required' => 'true' }

  spec.required_ruby_version = '>= 3.1'
  spec.add_runtime_dependency 'msgpack', '~> 1.7', '>= 1.7.2'
end

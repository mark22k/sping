# Splitted Ping Protocol Specification

This is a third-party specification of the Splitted Ping (sping) protocol version 3.0.

Splitted Ping is a network protocol (based on UDP and TCP) that provides peer-to-peer functionality and measures asymmetric latency and asymmetric loss.

The protocol was created by benjojo. He has explained the background of the protocol in his blog post [Splitting the ping](https://blog.benjojo.co.uk/post/ping-with-loss-latency-split).

## Introduction

Ping or other tools for measuring latency and packet loss provide an overview of how long packets take to get to a peer and back and whether packet loss occurs along the way. The time is generally specified in milliseconds. This time is recorded locally on the peer that initiated the measurement.

Asymmetric routing can occur in networks. This means that a packet takes a different outbound route than the return route. However, this can also mean that the outbound and return routes take different lengths of time. Classic applications such as ping can only measure the total latency and total packet loss, but not on which path this occurs.

The splitted ping protocol is intended to provide a remedy at precisely this point. It is intended to provide a way of making precise measurements for the outbound and return paths so that it is possible to see how long each path takes and where the packet loss occurs.

### Time

In the case of a measurement that only originates from one peer, this peer can measure the distance between sending the packet and receiving a response packet and can thus determine the latency.
For time measurement to be possible even with a split ping, the response packet must contain a time stamp indicating when the packet was sent. This can lead to problems if the time is not synchronized on both peers. The correctness of the time is irrelevant here, only that both peers have the same time.

## Protocol

### Protocol overview

The splitted ping protocol consists of three phases:

1. TCP handshake
2. UDP handshake
3. Sending time packets to each other

## Numbers

The splitted ping protocol uses TCP port 6924 and UDP port 6924, which were registered by benjojo at [IANA](https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml?search=6924).

## Session ID

A session ID is used to uniquely identify the connection between two peers. The session ID is represented by an unsigned 32-bit integer. This means that the session ID lies in a range between `0` and `(2^32)-1`.

## TCP handshake

The aim of the TCP handshake is to invite the peer and then determine the protocol version of the peer and negotiate a session ID.

In TCP and UDP handshakes, a distinction is made between the peer that initiates the session, referred to here as the client, and the peer that accepts the session, referred to here as the server.

The following steps are carried out to perform a TCP handshake:
- The client establishes a TCP connection to the server.
- The server accepts the TCP connection.
- The server sends the splitted ping banner.
- The client invites the server.
- The server accepts the invitation by responding with a session ID.
- The connection is closed.

A split ping banner looks like this:
```
sping-[VERSION]-[IMPLEMENTATION]\r\n
```
Here, `[VERSION]` has the value `0.3`.
`[IMPLEMENTATION]` is implementation-specific. A reference to the source code of the splitted ping implementation used is recommended.

For the current version of Splitted Ping and the use of the reference implementation, the banner looks as follows:
```
sping-0.3-https://github.com/benjojo/sping\r\n
```

The invitation sent by the client looks like this:
```
INVITE\r\n
```

The session ID has the following format:
```
[SESSION ID]\r\n
```
The session ID is displayed as a human-readable sequence of numbers.

The session is activated after a successful TCP handshake.

### Error handling

#### Error handling from the server

If the client sends an invitation message of more than 9000 characters, it can be assumed that the message is bogus. The connection is terminated.

If the client sends an incorrect invitation message, the I-don't-understand message is returned and the connection is closed.

The I-don't-understand message has the following content:
```
I_DONT_UNDERSTAND\r\n
```

#### Error handling from the client

If the server's banner is larger than 9000 characters, it can be assumed that the message is bogus. The connection is terminated.

If the banner does not correspond to the splitted ping banner, the connection is closed.

If the message containing the session ID is greater than 31 characters, the connection is terminated.

If the session ID is incorrect and therefore does not consist of human-readable numbers, the connection is terminated.

## UDP messages

The peers exchange UDP messages for both the UDP handshake and the time messages. These are coded with [MessagePack](https://msgpack.org/).
The UDP messages contain a string-object-mapping encoded with MessagePack. The string acts as a key. The object is dependent on the key. Each message must contain the following attributes:

Attribute `Y` (*Type*):
  This attribute contains the type. This is a one-digit character. The type is encoded as an integer. The Unicode number or the ASCII number for the respective character is used for this.

Attribute `M` (*Magic*):
  This attribute contains the magic number of the splitted ping protocol. The magic number is `11181`. This attribute is encoded as an integer.

Attribute `S` (*Session ID*):
  This attribute contains the session ID. This attribute is encoded as an integer.

All other attributes are type-specific.

## Error handling

If a message cannot be decoded with MessagePack, it is discarded.

If it does not contain the attributes, it is invalid and is discarded.

If an attribute is missing or is specified with the wrong type, the packet is discarded.

## UDP handshake

The UDP handshake comprises the following steps:
- The client sends a UDP handshake.
- The server receives it.
- The server confirms this by sending it back.
- The session is then double activated.
- If the client does not receive confirmation from the server, it sends the handshake again.

After the session has been activated, the UDP handshake is performed. To do this, the client sends the UDP handshake every second. As soon as the server has received and validated it, it sends it back. After the client has received the server's confirmation, the client stops sending the handshake and the session is double activated. If the session is not double activated within one minute, the entire session is discarded. If there is still interest in a session after this, a new session (starting with the TCP handshake) must be negotiated.

A handshake message has the type `h`. The following additional attributes are defined:

Attribute `V` (*Version*):
  This attribute contains the version of Splitted Ping. This is coded as an integer and is `3`.

After the double activation of a session, both peers know the splitted ping version of the other. The client via the TCP handshake with the server and the server with the UDP handshake from the client.

## Time messages

Time messages are UDP messages that represent a ping packet.

A time message contains the following additional attributes:

Attribute `I` (*ID*):
  The attribute contains a ping ID, from the time when the packet was created or sent. The ping ID is encoded as an integer.

Attribute `T` (*TX time*):
  The attribute contains the time at which the packet was created or sent. It is encoded as a timestamp with nanoseconds using the MessagePack timestamp extension.

Attribute `E` (*Sender error*):
  The attribute is currently not used. It contains `0`. It is encoded as an integer.

Attribute `A` (*Last acks*):
  This attribute contains the last 32 time messages which have been received. These are specified as an array of 32 ping infos. If no 32 time messages have been received, the array is filled with empty ping information.

## Receiving a time message

After receiving a time message, a ping info is created from this and saved. The last 32 ping infos are always saved.

The attributes of the ping info are assigned as follows:

Attribute `R` (*ID*):
  Ping ID of the received time message.

Attribute `U` (*TX time*):
  Time at which the peer sent the time message. This is stored in the time message under the attribute `T`.

Attribute `X` (*RX time*):
  Time at which the time message was received.

## Sending time messages

The peers send a time message to each other every second. The UDP handshake was used to ensure that no firewall blocks the time messages.

### Error handling

If the ping ID is invalid, the packet is discarded.

If the attribute `E` encodes an error (i.e. is not `0`), the packet is discarded.

If more or less than 32 ping infos have been specified, the packet is discarded.

## Ping info

A received time message is coded as ping info. As with UDP messages, MessagePack and a string-object-mapping is used.

A ping info has the following attributes:

Attribute `R` (*ID*):
  A ping ID is stored in this attribute. This attribute is encoded as an integer.

Attribute `U` (*TX time*):
  Time at which a time message was sent. It is encoded as a timestamp with nanoseconds using the MessagePack timestamp extension.

Attribute `X` (*RX time*):
  Time at which a time message was received. It is encoded as a nanosecond timestamp using the MessagePack timestamp extension.

### Empty ping info

An empty ping info has the ID `0`. Furthermore, both time stamps are set to the zero time.

### Error handling

If an attribute is missing in a ping info or is of the wrong type, the ping info and therefore the message it contained is invalid. Invalid messages are discarded.

## Ping ID

A ping ID is an unsigned 8-bit integer. A ping ID cannot be '0'.

A ping ID is calculated as follows:
```
([UNIX TIME] % 255) + 1
```
Where `[UNIX TIME]` is the Unix time in seconds.

The calculation ensures that a unique ID can be assigned to each second.

## Calculation of the latency time

When calculating the latency, a distinction is made between TX latency and RX latency. The TX latency is the latency between this peer and the other peer ("outbound") and the RX latency is the latency between the other peer and this peer (" return").

Note: If a negative integer is determined in a latency calculation, this would mean in context that a message was received before it was sent. This is not possible and generally indicates a faulty synchronization of the peers' clocks.

### Calculation of the TX latency

To calculate the TX latency, the last 32 time messages of the most recent message received by the peer are searched. Empty ping information is ignored. The most recent message is the one that contains the most advanced or highest TX time. The latency is then calculated by subtracting the TX time from the RX time (RX time - TX time) from this ping info.
If the time message only contains empty ping information, this means that the peer has not yet received a time message from us at the time of sending.

### Calculation of the RX latency

To calculate the RX latency, the time at which the message was received is subtracted from the time at which the time message was sent.

## Calculation of the packet loss

When calculating packet loss, a distinction is made between TX loss and RX loss. The TX loss is the number of packets lost to the other peer ("outbound") and the RX loss is the number of packets lost from the other peer to this peer (" return").

Note: The last 32 IDs can differ by one digit from the TX and RX loss. This is not an error.

### Calculation of the TX loss

The TX loss can only be calculated if 32 time messages have been sent and then a time message has been received.

To calculate the TX loss, the last 32 ping IDs that were sent are calculated. It then looks at how many of these IDs were missing in the last time message. The number is the packet loss.

### Calculation of the RX loss

The RX loss can only be calculated after 32 seconds. This means that the peer has sent us 32 time messages.

To calculate the RX loss, the last 32 ping IDs sent by the peer are calculated. We then check how many of these IDs are present in the last 32 ping info messages saved. The number of non-existent IDs is the packet loss.


# frozen_string_literal: true
# sharable_constant_value: literal

# SPing / Splitted Ping is a protocol for measuring asymmetric latencies.
module SPing
  # Inclusion of all possible SPing-specific errors.
  require_relative 'errors'

  # Container, which contains a collection of sessions and manages them.
  class SessionManager
    require 'socket'
    require 'timeout'
    require_relative 'session'

    # Creates a new Session Manager, which can (and will) manage a number of sessions.
    # @param host [#to_s] Host to which messages are to be bound and from which messages are sent accordingly.
    # @param port [#to_i] Port
    def initialize(host = '::', port = 6924)
      @host = host.to_s
      @port = port.to_i

      @sessions = {}
      @sessions_mutex = Mutex.new

      @socket = UDPSocket.new(Socket::AF_INET6)
      @socket.bind @host, @port
    end

    # Initiates a new session with a peer.
    # @param host [#to_s]
    # @param port [#to_s, #to_i]
    def new_session(host, port = 6924)
      $logger.info "Add new session for host #{host} port #{port}."
      session = SPing::Session.new host.to_s, port.to_i, @socket, true

      counter = 0
      loop do
        session_id = request_session session
        break if session_id

        # rubocop:disable Style/IfUnlessModifier
        if counter > 5
          raise OutOfSessions, 'The peer could not name a session ID that had not yet been assigned.'
        end
        # rubocop:enable Style/IfUnlessModifier

        counter += 1
      end

      $logger.info "TCP handshake for host #{host} port #{port} successful. Session ID is #{session.session_id}."

      $logger.info "Perform UDP handshake for session ID #{session.session_id}."
      session.start_udp_handshake_sender
    rescue Errno::ECONNRESET, EOFError, IO::TimeoutError, SocketError, TCPHandshakeError => e
      $logger.warn "TCP handshake failed: #{e.message}"
    rescue OutOfSessions => e
      $logger.error "Out of session: #{e.message}"
    end

    # Stops all threads connected to a session ID and removes the session from the Session Manager administration.
    # @param session_id [Integer] Session ID of the session to be removed.
    def del_session(session_id)
      $logger.debug "Delete session with session id #{session_id}"

      @sessions_mutex.synchronize do
        @sessions[session_id].stop
        @sessions.delete(session_id)
      end
    end

    # Stops all sessions or the threads that are connected to them.
    def stop_sessions
      @sessions.each do |_session_id, session|
        session.stop
      end
    end

    # Waits and blocks until the Session Manager is no longer running.
    def join
      @runner&.join
      @gc_thread&.join
      @server_thread&.join
    end

    # Starts the Session Manager.
    def run
      raise 'Client is already running.' if @runner
      raise 'GC is already running.' if @gc

      @runner = Thread.new do
        loop do
          Thread.new(@socket.recvfrom(10_000)) do |buf|
            handle_packet buf
          end
        end
      end
      @gc_thread = Thread.new do
        loop do
          do_gc
          sleep 10
        end
      end
    end

    # Starts the server functionality of the Session Manager.
    def run_server
      raise 'Server is already running.' if @server_thread
      raise 'Server already exist.' if @server

      @server = TCPServer.new @host, @port

      @server_thread = Thread.new do
        loop do
          Thread.new(@server.accept) do |client|
            handle_client client
          end
        end
      end

      return @server_thread
    end

    # Stops the server functionality of the Session Manager.
    def stop_server
      raise 'Server thread is not running.' unless @server_thread

      @server_thread.kill
      @server_thread = nil

      raise 'Server is not running.' unless @server

      @server.close
      @server = nil
    end

    # Stops the Session Manager. The server functionality must be stopped separately.
    def stop
      raise 'Session manager is not running.' unless @runner

      @runner.kill
      @runner = nil

      raise 'Session GC is not running.' unless @gc_thread

      @gc_thread.kill
      @gc_thread = nil
    end

    private

    # Generates a new session ID that was not yet in use at the time of the check.
    # If none could be generated within half a minute - for example because all
    # session IDs have already been assigned - an error is thrown.
    # @return [Integer]
    def generate_session_id
      Timeout.timeout(30, OutOfSessions, 'No session ID could be generated which has not yet been used.') do
        loop do
          session_id = rand (2**32) - 1
          return session_id unless @sessions.key? session_id
        end
      end
    end

    # Removes obsolete sessions. This includes sessions that are older than one minute but
    # have not been activated a double time. And sessions that have not
    # received any packets for half a minute.
    def do_gc
      $logger.debug 'Remove outdated sessions.'
      # rubocop:disable Style/HashEachMethods
      # You cannot run through a map and edit it at the same time. Since this is
      # necessary due to the threading,
      # a snapshot of the key variable is run through.
      @sessions.keys.each do |session_id|
        # rubocop:enable Style/HashEachMethods

        session = @sessions[session_id]
        # It is possible that sessions no longer exist here, as we may have
        # session IDs that have already been deleted.
        # However, we can ignore this aspect here, as we are the only
        # function that deletes sessions.
        current_time = Time.now.to_i
        if !(session.tcp_handshake_complete && session.udp_handshake_complete)
          # Handshake incomplete
          if (current_time - session.created.to_i) > 60
            # TCP and/or UDP take more than 60 seconds.
            $logger.debug "UDP handshake for session id #{session_id} timed out."
            del_session session_id
          end
        elsif (current_time - session.last_rx.to_i) > 30
          # 30 seconds have elapsed since the last ping from the peer was received.
          # The peer is probably dead.
          $logger.debug "Session id #{session_id} without activity for over thirty seconds."
          del_session session_id
        end
      end
    end

    # Receives a TCP handshake for a session and then has it
    # managed by the Session Manager.
    def request_session(session)
      session.do_tcp_handshake2

      @sessions_mutex.synchronize do
        unless @sessions.key? session.session_id
          @sessions[session.session_id] = session
          return session.session_id
        end
      end

      return nil
    end

    # Handler for a received packet. The function catches errors and outputs them.
    # It also forwards the packet to the corresponding handler according to the
    # session ID and type of packet.
    def handle_packet(buf)
      rxtime = Time.now

      peeraddr = buf[1][2]
      peerport = buf[1][1]
      $logger.debug "Packet received from #{peeraddr} port #{peerport}."

      packet = MessagePack.unpack(buf[0]).to_h

      if !packet.key?('M') ||
         !packet.key?('Y') ||
         !packet.key?('S') ||
         !packet['M'].is_a?(Integer) ||
         !packet['Y'].is_a?(Integer) ||
         !packet['S'].is_a?(Integer)
        raise InvalidMessageError, 'The peer has sent an invalid message. The packet is incorrectly coded.'
      end

      # Check whether the Magic Number is the correct one.
      if packet['M'] != 11_181
        raise UnknownPacketError, 'Package contains incorrect magic number. Processing is canceled.'
      end

      type = packet['Y'].chr
      case type
      when 'h'
        handle_udp_handshake packet, peeraddr, peerport
      when 't'
        handle_ping packet, rxtime, peeraddr, peerport
      else
        raise UnknownPacketError, "Package is of unknown type: #{type}"
      end
    rescue MessagePack::MalformedFormatError => e
      $logger.error "Packet cannot be decoded: #{e.message}"
    rescue PeerError => e
      $logger.error "Perr error: #{e.message}"
    end

    # Handler for a receiving UDP handshake.
    def handle_udp_handshake(packet, peeraddr, peerport)
      if !(packet.keys - %w[M Y V S]).empty? ||
         # M, Y are already checked in handle_packet from session manager
         !packet['V'].is_a?(Integer)
        raise InvalidMessageError, 'The peer has sent an invalid message. The handshake packet is incorrectly coded.'
      end

      session_id = packet['S']
      session = @sessions[session_id]
      raise UnknownSessionError, 'UDP handshake received for uninitiated session.' unless session
      raise UnsupportedVersionError, "UDP handshake uses an unsupported version: #{packet['V']}" if packet['V'] != 3

      session.set_endpoint peeraddr, peerport

      unless session.madebyme
        # If the session was not started by me, the other peer expects a
        # confirmation of the handshake in which the same is sent back.
        $logger.debug "Send UDP Handshake back for session id #{session_id}."
        session.send_udp_handshake
      end

      if session.udp_handshake_complete
        $logger.warn 'UDP handshake is received, although a previous one was already successful.'
        return
      end

      session.udp_handshake_recived
      $logger.info "UDP handshake for session ID #{session_id} was successful."

      session.start_pinger
    end

    # Handler for a receiving ping or a receiving time message.
    def handle_ping(packet, rxtime, peeraddr, peerport)
      session_id = packet['S']
      session = @sessions[session_id]
      unless session&.tcp_handshake_complete && session&.udp_handshake_complete
        raise UnknownSessionError, "Ping packet received for non-initiated session id #{session_id}."
      end

      session.set_endpoint peeraddr, peerport
      session.handle_ping packet, rxtime, peeraddr
    end

    # Handler to establish a session for the request of a peer.
    def handle_client(client)
      host = client.peeraddr[3]
      port = client.peeraddr[1]
      session = SPing::Session.new host, port, @socket, false

      $logger.debug "Session request from host #{host} port #{port}."

      session_id = nil

      @sessions_mutex.synchronize do
        session_id = generate_session_id
        @sessions[session_id] = session
      end

      session.do_tcp_handshake1 client, session_id
    rescue Errno::ECONNRESET, EOFError, IO::TimeoutError, SocketError, TCPHandshakeError => e
      $logger.warn "Could not establish a new session with Peer: #{e.message}"
      del_session session_id
    rescue OutOfSessions => e
      $logger.error "Could not establish a new session with Peer: #{e.message}"
      del_session session_id
    end
  end
end

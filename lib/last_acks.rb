# frozen_string_literal: true
# sharable_constant_value: literal

# SPing / Splitted Ping is a protocol for measuring asymmetric latencies.
module SPing
  # A kind of circular buffer, which consists of 32 acks at any given time. If no acks are available,
  # empty ones are created. If a new ack is added (which would result in 33 acks), one is removed.
  class LastAcks
    # Creates a new circular buffer for acks
    def initialize
      # Array in which the acks are stored.
      @acks = []
      # Mutex, which ensures that the array is not accessed simultaneously.
      @acks_mutex = Mutex.new

      # Initiallize the circular buffer with 32 empty acks.
      zero_time = Time.at(0)
      32.times do |index|
        @acks[index] = {
          'R' => 0,
          'U' => zero_time,
          'X' => zero_time
        }
      end
    end

    # Returns a copy of the last 32 acks.
    # @return [Array] Last 32 Acks
    def acks
      @acks_mutex.synchronize do
        return @acks.dup
      end
    end

    # Adds a new ack and deletes the oldest one
    # @param ack [Hash] New ack to be added
    def add_ack(ack)
      @acks_mutex.synchronize do
        @acks << ack
        @acks.shift
      end
    end
  end
end

# frozen_string_literal: true
# sharable_constant_value: literal

# SPing / Splitted Ping is a protocol for measuring asymmetric latencies.
module SPing
  # Inclusion of all possible SPing-specific errors.
  require_relative 'errors'

  # Models or represents a session with a peer.
  class Session
    # Time when the session was created. This is necessary so that the session GC knows when a
    # non-initialized session can be deleted.
    # @return [Time]
    attr_reader :created
    # Time when the last ping packet was received by the peer. This is important so that the session
    # GC knows when a session is considered inactive and can be deleted.
    # @return [Time]
    attr_reader :last_rx
    # Indicates whether the session has been initialized. true if we have initiated it.
    # false if the peer has initiated it.
    # @return [TrueClass, FalseClass]
    attr_reader :madebyme
    # Indicates whether a TCP handshake has been carried out (successfully).
    # @return [TrueClass, FalseClass]
    attr_reader :tcp_handshake_complete
    # Indicates whether a UDP handshake has been carried out (successfully).
    # @return [TrueClass, FalseClass]
    attr_reader :udp_handshake_complete
    # The session ID in the range from 0 to 2**32 - 1.
    # @return [Integer]
    attr_reader :session_id

    require 'socket'
    require 'timeout'
    require 'msgpack'
    require_relative 'last_acks'

    # Creates a new session
    # @param host [#to_s] Host of the peer
    # @param port [#to_i] Port of the peer
    # @param socket [UDPSocket] UDP socket via which the pings and the UDP handshake are to be sent.
    # @param madebyme [TrueClass, FalseClass] Indicates whether we have initiated the session.
    def initialize(host, port, socket, madebyme)
      # Assign the passed parameters to instance variables.
      @host = host.to_s
      @port = port.to_i
      @socket = socket
      @madebyme = madebyme

      # Initialize assignment of other instance variables
      @created = Time.now
      @double_activated = nil

      @tcp_handshake_complete = false
      @udp_handshake_complete = false

      @last_acks = SPing::LastAcks.new
    end

    # Initiates a TCP handshake.
    # @param socket [TCPSocket] Socket via which the handshake is to be sent
    #   (and responses are to be expected).
    # @param session_id [Integer] Session ID which is to be sent to the peer
    #   and which is to be used for the current session.
    def do_tcp_handshake1(socket, session_id)
      # Send the banner
      socket.write "sping-0.3-https://codeberg.org/mark22k/sping\r\n"
      # and make sure that it is no longer in the send buffer.
      socket.flush

      # See if the peer invites us to create a session with them.
      invite = socket.readpartial 6
      unless invite.chomp == 'INVITE'
        socket.write "I_DONT_UNDERSTAND\r\n"
        raise TCPHandshakeError, 'Peer didn\'t invite us.'
      end

      # Send the session ID
      @session_id = session_id
      socket.write "#{session_id}\r\n"
      # and make sure that it is no longer in the send buffer.
      socket.flush

      # This means that the TCP handshake is successful. The socket can be closed
      # and the corresponding instance variable can be set.
      socket.close

      @tcp_handshake_complete = true
    end

    # Receives a TCP handshake from a peer.
    # The peer specified when the session is created is consulted for this purpose.
    def do_tcp_handshake2
      # Establish a connection and receive the banner.
      socket = TCPSocket.new @host, @port

      # If the banner is too large, close the socket and throw an error.
      # The handshake was not successful.
      banner = socket.readpartial 9001
      if banner.length > 9000
        socket.close
        raise TCPHandshakeError, 'Host banner too big'
      end

      # If the banner does not match the SPing service, close the socket and
      # throw an error. The handshake was not successful.
      unless banner.start_with? 'sping-0.3-'
        socket.close
        raise TCPHandshakeError, 'Host banner not sping or unsupported version of sping.'
      end

      @remote_version = banner.chomp
      $logger.info "Peer uses the following program version: #{@remote_version.dump}"

      # Invite the peer to start a session with us.
      socket.write "INVITE\r\n"
      socket.flush

      # If the session ID is too long or none was received, close the socket and throw an error.
      invite_buf = socket.readpartial 32
      if invite_buf.length > 31 || invite_buf.empty?
        socket.close
        raise TCPHandshakeError, 'Invite banner wrong size'
      end

      @session_id = invite_buf.chomp.to_i

      socket.close

      @tcp_handshake_complete = true
    end

    # Sets the endpoint consisting of the host and port of the remote end. This is done
    # each time a new packet is received and ensures that the current endpoint is always available.
    # @param host [#to_s]
    # @param port [#to_i]
    def set_endpoint(host, port)
      @host = host.to_s
      @port = port.to_i
    end

    # Starts a thread which sends the UDP handshake at regular intervals.
    # @param send_interval [#to_i] The interval at which the UDP handshakes are to be sent.
    def start_udp_handshake_sender(send_interval = 1)
      raise 'UDP handshake sender is already running.' if @udp_handshake_sender

      @udp_handshake_sender = Thread.new(send_interval.to_i) do |th_send_interval|
        loop do
          begin
            send_udp_handshake
            sleep th_send_interval
          rescue SocketError => e
            $logger.warn "Failed to send ping: #{e.message}"
          end
        end
      end
    end

    # Send a single UDP handshake
    def send_udp_handshake
      packet = {
        'Y' => 'h'.ord,
        'M' => 11_181,
        'V' => 3,
        'S' => @session_id
      }.to_msgpack

      $logger.debug "Send UDP handshake to #{@host} port #{@port}."
      @socket.send packet, 0, @host, @port
    end

    # Stop the UDP handshake sender. UDP handshakes are then no longer sent at any interval.
    def stop_udp_handshake_sender
      raise 'UDP handshake sender is not running and therefore cannot be terminated.' unless @udp_handshake_sender

      @udp_handshake_sender.kill
      @udp_handshake_sender = nil
    end

    # Informs the session that a UDP handshake has been received.
    def udp_handshake_recived
      stop_udp_handshake_sender if @madebyme
      @udp_handshake_complete = true
      @double_activated = Time.now
    end

    # Starts a thread which sends ping or time messages to the peer at one-second intervals.
    def start_pinger
      raise 'Pinger is already running.' if @pinger

      @pinger = Thread.new do
        loop do
          ping
          sleep 1
        end
      end
    end

    # Stops the thread, which sends ping or time messages to the peer at regular intervals.
    def stop_pinger
      raise 'Pinger sender is not running and therefore cannot be terminated.' unless @pinger

      @pinger.kill
      @pinger = nil
    end

    # Sends a ping message to the peer.
    def ping
      current_id = (Time.now.to_i % 255) + 1
      data = {
        'Y' => 't'.ord,
        'M' => 11_181,
        'S' => @session_id,
        'I' => current_id,
        'T' => Time.now,
        'E' => 0,
        'A' => @last_acks.acks
      }.to_msgpack

      $logger.debug "Send ping to #{@host} port #{@port}."
      @socket.send data, 0, @host, @port
    end

    # Stops all threads associated with the session. This means that the session to the peer is as good as dead.
    def stop
      @pinger&.kill
      @udp_handshake_sender&.kill
    end

    # Handler that receives and processes a receiving ping packet or time message. The current statistics are output.
    # @param packet [Hash]
    # @param rxtime [Time]
    # @param peeraddr [#to_s]
    def handle_ping(packet, rxtime, peeraddr)
      if !(packet.keys - %w[M Y E I T A S]).empty? ||
         # M, Y are already checked in handle_packet from session manager
         !packet['E'].is_a?(Integer) ||
         !packet['I'].is_a?(Integer) ||
         !packet['T'].is_a?(Time) ||
         !packet['A'].is_a?(Array)
        raise InvalidPacketError, 'The peer has sent an invalid message. The ping packet is incorrectly coded.'
      end

      raise SignalizedError, "Package displays an error message: #{packet['E']} Processing aborted." if packet['E'] != 0

      id = packet['I']
      txtime = packet['T']
      remote_last_acks = packet['A']

      if remote_last_acks.length != 32
        raise InvalidMessageError, 'The peer has sent an invalid message. It does not contain any 32-Acks.'
      end

      remote_last_acks.each do |block_ack|
        if !(block_ack.keys - %w[R U X]).empty? ||
           !block_ack['R'].is_a?(Integer) ||
           !block_ack['U'].is_a?(Time) ||
           !block_ack['X'].is_a?(Time)
          raise InvalidMessageError, 'The peer has sent an invalid message. An Ack is formatted invalid.'
        end
      end

      ack = {
        'R' => id,
        'U' => txtime,
        'X' => rxtime
      }

      @last_acks.add_ack(ack)
      @last_rx = rxtime

      # This is the A
      # The peer is B
      # TX => Time from A to B
      # RX => Time from B to A

      # Remove zero acks
      remote_last_acks.reject! { |block_ack| block_ack['R'].zero? }

      # Sort by TX time
      remote_last_acks.sort_by! { |block_ack| block_ack['U'] }

      newest_remote_ack = remote_last_acks.last.to_h

      # Calculate loss
      tx_loss = 0
      rx_loss = 0
      exchanges = 0

      last_local_acks = @last_acks.acks

      # A calculation can only take place when enough data is available, i.e. when:
      # - We have sent 32 time messages (this is the case after 32 seconds)
      # - The peer has sent 32 time messages (this is the case after 32 seconds)
      # - If there is a time message (this is the case when we are in this function)
      if @double_activated && (Time.now - @double_activated).to_i > 32
        # We have enough data
        exchanges = 32
        remote_acks = remote_last_acks.map { |ack| ack['R'] }
        local_acks = last_local_acks.map { |ack| ack['R'] }

        tip_id = (Time.now.to_i % 255) + 1
        starting = tip_id - 32

        last_ids = if tip_id > 32
                     (starting..tip_id).to_a
                   else
                     ((255 + starting)..255).to_a + (1..tip_id).to_a
                   end

        last_ids[0...-1].each do |id|
          tx_loss += 1 unless remote_acks.include? id
        end

        last_ids[1..].each do |id|
          rx_loss += 1 unless local_acks.include? id
        end
      end

      tx_latency = ((newest_remote_ack['X'].to_f - newest_remote_ack['U'].to_f) * 1000.0).round(6)
      rx_latency = ((rxtime - txtime) * 1000.0).round(6)
      puts "[#{peeraddr}] RX: #{rx_latency}ms TX: #{tx_latency}ms [Loss RX: #{rx_loss}/#{exchanges} | Loss TX: #{tx_loss}/#{exchanges}]"
    end
  end
end

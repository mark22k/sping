# frozen_string_literal: true
# sharable_constant_value: literal

# SPing / Splitted Ping is a protocol for measuring asymmetric latencies.
module SPing
  # Generic SPing error.
  class SPingError < StandardError; end

  # Error that occurred while managing the sessions.
  class SessionManagerError < SPingError; end

  # Error that is thrown when there are not enough session IDs available to establish a
  # connection to a peer or to accept new ones.
  # This indicates either a programmer error or an overload of the server.
  class OutOfSessions < SessionManagerError; end

  # Error that occurs in connection with a session.
  class SessionError < SPingError; end

  # Error during the TCP handshake
  class TCPHandshakeError < SessionError; end

  # Error due to the peer (i.e. not self-inflicted).
  class PeerError < SPingError; end

  # Package was not coded correctly.
  class InvalidPacketError < PeerError; end

  # The package was coded correctly, but the content is invalid.
  class InvalidMessageError < InvalidPacketError; end

  # A packet has been received which cannot be clearly assigned to SPing
  # and therefore cannot be processed further.
  class UnknownPacketError < InvalidPacketError; end

  # The peer has signaled an error.
  class SignalizedError < PeerError; end

  # The peer uses a version of sping that is not supported.
  class UnsupportedVersionError < PeerError; end

  # The package could not be assigned to a current session.
  class UnknownSessionError < PeerError; end
end

# sping

Programm zur Messung asymmetrischen Latenzen.

Dies ist eine Reimplementierung in Ruby von der Referenzimplementierung des sping Protokolls in Go. Das Programm stellt sowohl den Client als auch Serverteil bereit um zwsichen zwei Peers asymetrische Latenzen und asymetrischen Paketverlust zu messen.

## Protokoll

Das Protokoll besteht aus drei Phasen:
1. Einem TCP-Handschlag
2. Einem UDP-Handschlag
2. Dem Senden der Pings

Als Erstes wird der TCP-Handschlag durchgeführt. Hierbei handeln die Peers eine Sitzungs-ID aus. Dafür sendet der Client eine Anfrage an den Server und lädt ihn anschließend zu einer Sitzung ein. Wenn der Server die Einladung annimmt, sendet er eine Sitzungs-ID an den Client. Diese dient dazu, die Verbindung zwischen den Peers eindeutig zu identifizieren. Beispielsweise kann es vorkommen, dass ein Peer seinen Port oder seine IP-Adresse ändert. Damit sich die Peers weiterhin gegenseitig identifizieren können, gibt es die Sitzungs-ID. Am Ende des TCP-Handschlags ist die Sitzung aktiviert.

Danach wird ein UDP-Handschlag durchgeführt. Bei dieser sendet der Client ein UDP-Paket, welches mit [MessagePack](https://msgpack.org/) kodiert ist, an den Server. Dieser sendet das Paket zur Bestätigung an den Client zurück. Dies dient unter anderem dazu, ein Loch in eine Firewall zu schlagen, sodass UDP-Pakete zwischen Peers durchlassen werden. Nach einem erfolgreichen UDP-Handschlag ist die Sitzung doppelt aktiviert. Erst danach geht das Versenden von Ping-Paketen los.

Als Letztes senden sich die Peers gegenseitig Pings zu (in diesem Fall UDP-Pakete, welche Zeit-Nachrichten enthalten). Diese Zeit-Nachrichten beinhalten unter anderem den Zeitpunkt, wann die Nachricht gesendet worden ist, aber auch die letzten 32 empfangenen Zeit-Nachrichten des Peers. Durch diese Informationen können sowohl die asymmetrische Latenz als auch der asymmetrische Paketverlust ermittelt werden.

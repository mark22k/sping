# Splitted Ping Protokollspezifikation

Dies ist eine durch Dritte erstellte Spezifikation des Splitted Ping (sping)-Protokolls Version 3.0.

Splitted Ping ist ein Netzwerkprotokoll (aufbauend auf UDP und TCP), welches eine Peer-to-Peer-Funktionalität bereitstellt und asymmetrische Latenzen und den asymmetrischen Verlust zu messen.

Das Protokoll wurde von benjojo erstellt. Dieser hat die Hintergründe des Protokolls in seinem Blogpost [Splitting the ping](https://blog.benjojo.co.uk/post/ping-with-loss-latency-split) dargestellt.

## Einleitung

Ping oder andere Tools zur Messung von Latenzen und Paketverlust stellen eine Übersicht bereit, wie lange Pakete zu einem Peer und Zurück brauchen bzw. ob es auf diesem Weg zu einem Paketverlust kommt. Die Zeit wird dabei allgemein in Millisekunden angegeben. Diese Zeit wird dabei lokal auf dem Peer aufgezeichnet, welcher die Messung eingeleitet hat.

In Netzwerken kann es zu asymmetrischem Routing kommen. Dies bedeutet, dass ein Paket einen anderen Hinweg als Rückweg nimmt. Dies kann aber auch bedeuten, dass die Hin- und Rückwege unterschiedlich lange dauern. Klassische Anwendungen wie ping können nur die Gesamtlatenz und Gesamtpaketverlust messen, jedoch nicht auf welchem Weg dieser auftritt.

Das Splitted Ping-Protokoll soll genau an dieser Stelle Abhilfe schaffen. Es soll eine Möglichkeit bereitstellen, genaue Messungen für Hin- und Rückweg anzustellen, sodass erkennbar ist, wie lange welcher Weg dauert bzw. wo der Paketverlust auftritt.

### Zeit

Bei einer Messung, welche nur von einem Peer ausgeht, kann dieser den Abstand zwischen dem Senden des Pakets und dem Empfang eines Antwortpakets messen und kann dadurch die Latenz ermitteln.
Damit eine Zeitmessung auch bei einem gespalteten Ping möglich ist, muss im Antwortpaket ein Zeitstempel enthalten sein, wann das Paket gesendet worden ist. Hierbei kann es zu Problemen kommen, wenn die Uhrzeit auf beiden Peers nicht synchron ist. Die Richtigkeit der Uhrzeit ist dabei irrelevant, lediglich dass beide Peers die gleiche Uhrzeit haben.

## Protokoll

### Protokollübersicht

Das Splitted Ping-Protokoll besteht aus drei Phasen:

1. TCP-Handschlag
2. UDP-Handschlag
3. Gegenseitiges Zusenden von Zeitpaketen

## Nummern

Das Splitted Ping-Protokoll verwendet TCP-Port 6924 und UDP-Port 6924. Diese wurden dafür von benjojo bei der [IANA](https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml?search=6924) registriert.

## Sitzungs-ID

Eine Sitzungs-ID (englisch: Session ID) dient dazu, die Verbindung zwischen zwei Peers eindeutig zu identifizieren. Die Session ID wird dabei durch eine vorzeichenlose 32-Bit-Ganzzahl dargestellt. Dies bedeutet, dass die Sitzungs-ID in einem Bereich zwischen `0` und `(2^32)-1` liegt.

## TCP-Handschlag

Ziel des TCP-Handschlages (englisch: TCP handshake) ist es, den Peer einzuladen und anschließend die Protokollversion des Peers zu ermitteln und eine Session ID auszuhandeln.

Beim TCP- und UDP-Handschlag wird zwischen dem Peer, welcher die Sitzung einleitet, hier Client genannt, und dem Peer, welcher die Sitzung akzeptiert, hier Server genannt, unterschieden.

Um einen TCP-Handschlag durchzuführen, werden folgende Schritte durchgeführt:
- Der Client baut eine TCP-Verbindung zum Server auf.
- Der Server akzeptiert die TCP-Verbindung.
- Der Server sendet den Splitted Ping-Banner.
- Der Client lädt den Server ein.
- Der Server akzeptiert die Einladung, indem er mit einer Sitzungs-ID antwortet.
- Die Verbindung wird geschlossen.

Ein Splitted Ping Banner sieht wie folgt aus:
```
sping-[VERSION]-[IMPLEMENTATION]\r\n
```
Dabei hat `[VERSION]` den Wert `0.3`.
`[IMPLEMENTATION]` ist dabei implementationsspezifisch. Es wird ein Verweis auf den Quellcode der verwendeten Splitted Ping-Implementation empfohlen.

Für die aktuelle Version von Splitted Ping und der Verwendung der Referenzimplementierung sieht der Banner wie folgt aus:
```
sping-0.3-https://github.com/benjojo/sping\r\n
```

Die Einladung des Clients sieht wie folgt aus:
```
INVITE\r\n
```

Die Sitzungs-ID hat folgendes Format haben:
```
[SESSION ID]\r\n
```
Dabei wird die Sitzungs-ID als menschenlesbare Zahlenfolge dargestellt.

Nach erfolgreichem TCP-Handschlag ist die Sitzung aktiviert.

### Fehlerbehandlung

#### Fehlerbehandlung vom Server

Wenn der Client eine Einladungsnachricht von mehr als 9000 Zeichen sendet, kann angenommen werden, dass die Nachricht Bogus ist. Die Verbindung wird abgebrochen.

Wenn der Client eine falsche Einladungsnachricht sendet, wird die Ich-Verstehe-Nicht-Meldung zurückgesendet und die Verbindung geschlossen.

Die Ich-Verstehe-Nicht-Meldung hat folgenden Inhalt:
```
I_DONT_UNDERSTAND\r\n
```

#### Fehlerbehandlung vom Client

Wenn der Banner des Servers größer als 9000 Zeichen ist, kann angenommen werden, dass die Nachricht Bogus ist. Die Verbindung wird abgebrochen.

Wenn die Banner nicht dem Splitted Ping-Banner entspricht, wird die Verbindung geschlossen.

Wenn die Nachricht, welche die Sitzungs-ID beinhaltet, größer als 31-Zeichen ist, wird die Verbindung abgebrochen.

Wenn die Sitzungs-ID falsch ist, daher nicht aus menschenlesbaren Zahlen besteht, wird die Verbindung abgebrochen.

## UDP Nachrichten

Die Peers tauschen sowohl für den UDP-Handschlag als auch für die Zeit-Nachrichten UDP-Nachrichten aus. Diese werden mit [MessagePack](https://msgpack.org/) kodiert.
Die UDP-Nachrichten enthalten dabei ein mit MessagePack kodiertes String-Objekt-Mapping. Dabei agiert der String als Schlüssel. Das Objekt ist vom Schlüssel abhängig. Folgende Attribute muss jede Nachricht enthalten:

Attribut `Y` (*Type*):
  Dieses Attribut beinhaltet den Typ. Dieses ist ein einstelliges Zeichen. Der Typ wird als Ganzzahl kodiert. Hierfür wird die Unicode-Zahl oder die ASCII-Zahl für das jeweilige Zeichen kodiert.

Attribut `M` (*Magic*):
  Dieses Attribut beinhaltet die Magic Number des Splitted Ping-Protokolls. Die Magic Number ist `11181`. Dieses Attribut wird als Ganzzahl kodiert.

Attribut `S` (*Session ID*):
  Dieses Attribut enthält die Sitzungs-ID. Dieses Attribut wird als Ganzzahl kodiert.

Alle anderen Attribute sind typspezifisch.

## Fehlerbehandlung

Wenn eine Nachricht nicht mit MessagePack dekodiert werden kann, wird sie verworfen.

Wenn sie die Attribute nicht beinhaltet, ist sie falsch und wird verworfen.

Wenn ein Attribut fehlt oder mit dem falschen Typ angegeben wird, wird das Paket verworfen.

## UDP-Handschlag

Der UDP-Handschlag umfasst folgende Schritte:
- Der Client sendet einen UDP-Handschlag.
- Der Server empfängt diesen.
- Der Server bestätigt diesen, indem er ihn zurücksendet.
- Die Sitzung ist damit doppelt aktiviert.
- Falls der Client keine Bestätigung vom Server bekommt, sendet er den Handschlag erneut.

Nachdem die Sitzung aktiviert wurde, wird der UDP-Handschlag durchgeführt. Dafür sendet der Client jede Sekunde den UDP-Handschlag. Sobald der Server diesen empfangen und validiert hat, sendet er ihn zurück. Nachdem der Client die Bestätigung des Servers erhalten hat, hört der Client auf, den Handschlag zu senden, und die Sitzung ist doppelt aktiviert. Wenn die Sitzung nicht innerhalb von einer Minute doppelt aktiviert ist, wird die gesamte Sitzung verworfen. Wenn danach immer noch Interesse an einer Sitzung besteht, muss eine neue Sitzung (anfangend mit dem TCP-Handschlag) ausgehandelt werden.

Eine Handschlag-Nachricht hat den Typ `h`. Folgende zusätzliche Attribute sind definiert:

Attribut `V` (*Version*):
  Dieses Attribut enthält die Version von Splitted Ping. Diese wird als Ganzzahl kodiert und ist `3`.

Nach der doppelten Aktivierung einer Sitzung sind beiden Peers die Splitted Ping Version des jeweils anderen bekannt. Dem Client über den TCP-Handschlag mit dem Server und dem Server mit dem UDP-Handschlag vom Client.

## Zeit-Nachrichten

Zeit-Nachrichten sind UDP-Nachrichten, die ein Ping-Paket darstellen.

Eine Zeit-Nachricht beinhaltet folgende zusätzliche Attribute:

Attribut `I` (*ID*):
  Das Attribut beinhaltet eine Ping-ID, vom Zeitpunkt, wo das Paket erstellt bzw. versendet worden ist. Die Ping-ID wird als Ganzzahl kodiert.

Attribut `T` (*TX time*):
  Das Attribut beinhaltet den Zeitpunkt, an welchem das Paket erstellt bzw. versendet worden ist. Es wird mithilfe der Zeitstempelerweiterung von MessagePack als Zeitstempel mit Nanosekunden kodiert.

Attribut `E` (*Senders error*):
  Das Attribut wird aktuell nicht verwendet. Es beinhaltet `0`. Es wird als Ganzzahl kodiert.

Attribut `A` (*Last acks*):
  Dieses Attribut beinhaltet die letzten 32 Zeit-Nachrichten, welche empfangen worden sind. Diese werden als eine Array aus 32 Ping-Infos angegeben. Wenn noch keine 32 Zeit-Nachrichten erhalten wurden, wird die Array mit leeren Ping-Infos ausgefüllt.

## Empfang von einer Zeit-Nachricht

Nach Empfang von einer Zeit-Nachricht wird aus dieser eine Ping-Info hergestellt und abgespeichert. Dabei bleiben immer die letzten 32 Ping-Infos gespeichert.

Die Attribute der Ping-Info werden wie folgt belegt:

Attribut `R` (*ID*):
  Ping-ID der empfangenden Zeit-Nachricht.

Attribut `U` (*TX time*):
  Zeitpunkt, an welchem der Peer die Zeit-Nachricht abgesendet hat. Dieser ist in der Zeit-Nachricht unter dem Attribut `T` gespeichert.

Attribut `X` (*RX time*):
  Zeitpunkt, an welchem die Zeit-Nachricht empfangen wurde.

## Senden von Zeit-Nachrichten

Die Peers senden sich jede Sekunde eine Zeit-Nachricht zu. Durch den UDP-Handschlag wurde versucht sicherzustellen, dass keine Firewall die Zeit-Nachrichten blockiert.

### Fehlerbehandlung

Wenn die Ping-ID falsch ist, wird das Paket verworfen.

Wenn das Attribut `E` einen Fehler kodiert (also nicht `0` ist), wird das Paket verworfen.

Wenn mehr oder weniger als 32 Ping-Infos angegeben worden sind, wird das Paket verworfen.

## Ping-Info

Eine empfangende Zeit-Nachricht wird als Ping-Info kodiert. Dabei wird wie bei UDP-Nachrichten MessagePack und ein String-Objekt-Mapping verwendet.

Eine Ping-Info hat folgende Attribute:

Attribut `R` (*ID*):
  In diesem Attribut wird eine Ping-ID gespeichert. Dieses Attribut wird als Ganzzahl kodiert.

Attribut `U` (*TX time*):
  Zeitpunkt, an welchem eine Zeit-Nachricht gesendet wurde. Es wird mithilfe der Zeitstempelerweiterung von MessagePack als Zeitstempel mit Nanosekunden kodiert.

Attribut `X` (*RX time*):
  Zeitpunkt, an welchem eine Zeit-Nachricht empfangen wurde. Es wird mithilfe der Zeitstempelerweiterung von MessagePack als Zeitstempel mit Nanosekunden kodiert.

### Leere Ping-Info

Eine leere Ping-Info hat die ID `0`. Des Weiteren sind beide Zeitstempel auf den Null-Zeitpunkt gesetzt.

### Fehlerbehandlung

Wenn ein Attribut in einer Ping-Info fehlt oder vom falschen Typ ist, ist die Ping-Info und damit die Nachricht, welche sie enthalten hat, falsch. Falsche Nachrichten werden verworfen.

## Ping-ID

Eine Ping-ID ist ein vorzeichenloser 8-Bit Ganzzahl. Eine Ping-ID kann nicht `0` sein.

Eine Ping-ID wird die folgt berechnet:
```
([UNIX TIME] % 255) + 1
```
Dabei ist `[UNIX TIME]` die Unix-Zeit in Sekunden.

Durch die Berechnung ist sichergestellt, dass jeder Sekunde eine eindeutige ID zugeordnet werden kann.

## Berechnung der Latenzzeit

Bei der Berechnung der Latenzzeit wird zwsichen der TX-Latenz und der RX-Latenz unterschieden. Die TX-Latenz ist dabei die Latenz zwischen diesem Peer zum anderen (Hinweg) und die RX-Latenz ist die Latenz des anderen Peer und diesem (Rückweg).

Anmerkung: Wenn bei einer Latenzzeitberechnung eine negative Ganzzahl ermittelt wird, würde dies im Kontext bedeutet, dass eine Nachricht empfangen wurde, bevor diese versendet wurde. Dies ist nicht möglich und deutet im Allgemeinen auf eine fehlerhafte Synchronisation der Uhren der Peers hin.

### Berechnung der TX-Latenz

Um die TX-Latenz zu berechnen, werden die letzten 32 vom Peer empfangenden Zeit-Nachrichten der aktuellesten Nachricht durchsucht. Dabei werden leere Ping-Infos ignoriert. Die aktuellste Nachricht ist dabei die, die die fortgeschrittenste bzw. höchste TX-Zeit beinhaltet. Die Latenz setzt sich dann aus der Subtraktion TX-Zeit von der RX-Zeit (RX Zeit - TX Zeit) von dieser Ping-Info zusammen.
Wenn die Zeit-Nachricht nur leere Ping-Infos beinhaltet, bedeutet das, dass der Peer zum Zeitpunkt des Absendens noch keine Zeit-Nachricht von uns erhalten hat.

### Berechnung der RX-Latenz

Für die Berechnung der RX-Latenz wird der Zeitpunkt, wo die Nachricht empfangen wurde, mit dem Zeitpunkt, wo die Zeit-Nachricht gesendet wurde, subtrahiert.

## Berechnung des Paketverlustes

Bei der Berechnung des Paketverlustes wird zwischen dem TX-Verlust und dem RX-Verlust unterschieden. Der TX-Verlust ist dabei die Anzahl der Pakete, welche zum anderen Peer verloren gegangen sind (Hinweg) und der RX-Verlust ist die Anzahl der Pakete, welche vom anderen Peer zu diesem verloren gegangen sind (Rückweg).

Anmerkung: Die letzten 32 IDs können sich um eine Stelle vom TX- und RX-Verlust unterscheiden. Dies ist kein Fehler.

### Berechnung des TX-Verlustes

Die Berechnung der TX-Verlustes kann erst erfolgen, wenn 32 Zeit-Nachrichten gesendet worden und danach eine Zeit-Nachricht erhalten worden ist.

Zur Berechnung des TX-Verlustes werden die letzten 32 Ping-IDs berechnet, welche versendet worden sind. Danach wird geschaut, wie viele von diesen IDs in der letzten Zeit-Nachricht fehlten. Die Anzahl ist der Paketverlust.

### Berechnung des RX-Verlustes

Die Berechnung der RX-Verlustes kann erst nach 32 Sekunden erfolgen. Dies bedeutet, dass der Peer uns 32 Zeit-Nachrichten gesendet hat.

Zur Berechnung des RX-Verlustes werden die letzten 32 Ping-IDs berechnet, welche der Peer versendet hat. Danach wird geschaut, wie viele dieser IDs in den letzten abgespeicherten 32 Ping-Infos vorhanden sind. Die Anzahl der nicht vorhandenen IDs ist der Paketverlust.

